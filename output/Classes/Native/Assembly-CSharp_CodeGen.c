﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void HoldButton::Start()
extern void HoldButton_Start_mBF83F0E934C0BC78B4A675D31C328B818B8BA8AA ();
// 0x00000002 System.Void HoldButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void HoldButton_OnPointerDown_m1B7CCEE563D8DE2E9966D7B765DBF44EB173B844 ();
// 0x00000003 System.Void HoldButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void HoldButton_OnPointerUp_m4A244144ABF324D95F2AFB5A4D0E9A4C49B42BF7 ();
// 0x00000004 System.Boolean HoldButton::isPressed()
extern void HoldButton_isPressed_m837C8FEE84F921E92091CD5628907676C2155F09 ();
// 0x00000005 System.Void HoldButton::.ctor()
extern void HoldButton__ctor_m7AD08A6AAABDA0A3E8B09A6F08C5AF8E4448CC69 ();
// 0x00000006 System.Void ImageChangingButton::Start()
extern void ImageChangingButton_Start_m88DD1213E90EB79DA3AD9D6588ACAFDD60FCD744 ();
// 0x00000007 System.Void ImageChangingButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ImageChangingButton_OnPointerDown_mBA84458244A8291BCDF29EE96918FACDC876A745 ();
// 0x00000008 System.Void ImageChangingButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void ImageChangingButton_OnPointerUp_m224607CA1FBC7BA237D8119AE636D135670C530C ();
// 0x00000009 System.Void ImageChangingButton::.ctor()
extern void ImageChangingButton__ctor_mA7DEAE869B41C2CE0937A4FD8B21B00E24BCFF67 ();
// 0x0000000A MusicPlayer MusicPlayer::get_Instance()
extern void MusicPlayer_get_Instance_mBF96BD3397BC310FA231B83F358086DCA224E855 ();
// 0x0000000B System.Void MusicPlayer::Awake()
extern void MusicPlayer_Awake_mB58A606103D324532830131CA4A73C00B99E4C7A ();
// 0x0000000C System.Void MusicPlayer::PlayMusic()
extern void MusicPlayer_PlayMusic_mE3F3ECCD025172827E90D20919C0699D86EE90CD ();
// 0x0000000D System.Void MusicPlayer::StopMusic()
extern void MusicPlayer_StopMusic_mF6DD0067F9A3730D289A83E0B4F2519DFFE676F0 ();
// 0x0000000E System.Void MusicPlayer::.ctor()
extern void MusicPlayer__ctor_mE89B163EA72BF12E0B7D28E30407AF24E94741BF ();
// 0x0000000F System.Void MusicPlayer::.cctor()
extern void MusicPlayer__cctor_m01C6940307C44F6B6CC29C16CED4CBF995DE8734 ();
// 0x00000010 System.Void PlayerController::Start()
extern void PlayerController_Start_mC0C9B9461D0BDAC48EC43715818A4BA63C4F45EF ();
// 0x00000011 System.Void PlayerController::Update()
extern void PlayerController_Update_m38903EF1C8F12B9388303741F8040EE26C33DC33 ();
// 0x00000012 System.Void PlayerController::accelerate()
extern void PlayerController_accelerate_m13F44234BA301D6859140FE35B3886BE4DDCEC0D ();
// 0x00000013 System.Void PlayerController::rotate(System.Single)
extern void PlayerController_rotate_m9B5E181B0F5BD743896C5C07D55F7E6FCEFC97F7 ();
// 0x00000014 System.Void PlayerController::handleHolding()
extern void PlayerController_handleHolding_mA03FA819CA48D04CDA3E2FE87820D3C6D13598EF ();
// 0x00000015 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_m648E40092E37395F4307411E855445886113CD60 ();
// 0x00000016 System.Void SceneController::Start()
extern void SceneController_Start_m1A1FAC171D097231B7D7A3BEE3BC93FF49F759FF ();
// 0x00000017 System.Void SceneController::Update()
extern void SceneController_Update_m0C714789D1D87A7B86C9684D036F9A2E7F9B338E ();
// 0x00000018 System.Void SceneController::ResetScene()
extern void SceneController_ResetScene_mD33EFE9411F4D88B36520386561D7EC914F6D39A ();
// 0x00000019 System.Void SceneController::.ctor()
extern void SceneController__ctor_mDE5EC139B719C6585BDFA1AE15234C5848FE8D01 ();
// 0x0000001A System.Void ShipConnectionPoint::Start()
extern void ShipConnectionPoint_Start_mA21AADE789A0EEE98518AC663FE0E919A2F44BA3 ();
// 0x0000001B System.Void ShipConnectionPoint::Update()
extern void ShipConnectionPoint_Update_mF125726E3569EF0C185AAAA46EB24C9E658A2A14 ();
// 0x0000001C System.Boolean ShipConnectionPoint::isConnectedToCorrespondingPoint()
extern void ShipConnectionPoint_isConnectedToCorrespondingPoint_m84D1AA9321DF5179185D1CA91D451E928228CB93 ();
// 0x0000001D System.Void ShipConnectionPoint::.ctor()
extern void ShipConnectionPoint__ctor_mFA1903D40E9AE3A80E0D335B4B61F42ABF18BB80 ();
// 0x0000001E System.Void ShipController::Start()
extern void ShipController_Start_m6AEAA5CD9EA713B204194B3A8FFA90E7142A9B8A ();
// 0x0000001F System.Void ShipController::Update()
extern void ShipController_Update_m9413942EDB9440735D42B115F999B0924ED8D086 ();
// 0x00000020 System.Collections.IEnumerator ShipController::TransitionToWinScene()
extern void ShipController_TransitionToWinScene_m11B19D0C80DA56BF6557BD0AA8CE23F1423CCD4A ();
// 0x00000021 System.Void ShipController::.ctor()
extern void ShipController__ctor_m8090CC282CD23B0D2C8A090E263A00EF44070366 ();
// 0x00000022 System.Void SlideBarControl::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void SlideBarControl_OnPointerUp_m54FD6F19FE26D4A76BCF477ED6F5CBF48AD58691 ();
// 0x00000023 System.Single SlideBarControl::GetValue()
extern void SlideBarControl_GetValue_m05069915FECAC42AE48F3472C5B133A1CB4A83B9 ();
// 0x00000024 System.Void SlideBarControl::Start()
extern void SlideBarControl_Start_mC9193599DE8A112647B4550FFE2149D2B5E25F00 ();
// 0x00000025 System.Void SlideBarControl::.ctor()
extern void SlideBarControl__ctor_mF47A8579854AFF144622CC98576B9F3D56E16CB3 ();
// 0x00000026 System.Void TitleScreenController::PlayGame()
extern void TitleScreenController_PlayGame_m6540FD2BF81AF7ED8E3C7982DCE2EE64C98ECA45 ();
// 0x00000027 System.Void TitleScreenController::GoToCredits()
extern void TitleScreenController_GoToCredits_m7E10B65732483BDF2AB8F505D501827CAD04C791 ();
// 0x00000028 System.Void TitleScreenController::GoToTitleScreen()
extern void TitleScreenController_GoToTitleScreen_mE6201EC69F7C96933820712FE977EDF7C48BF19E ();
// 0x00000029 System.Void TitleScreenController::.ctor()
extern void TitleScreenController__ctor_m725EE47F44118059E3FF7FC794CE549B0DC0DED1 ();
// 0x0000002A System.Void ShipController_<TransitionToWinScene>d__7::.ctor(System.Int32)
extern void U3CTransitionToWinSceneU3Ed__7__ctor_m0037C51497BD3C27A8E1C69519026A6144286819 ();
// 0x0000002B System.Void ShipController_<TransitionToWinScene>d__7::System.IDisposable.Dispose()
extern void U3CTransitionToWinSceneU3Ed__7_System_IDisposable_Dispose_mF76A0F8AD4AA67C2A0D93801A152CA50C3CBA7E9 ();
// 0x0000002C System.Boolean ShipController_<TransitionToWinScene>d__7::MoveNext()
extern void U3CTransitionToWinSceneU3Ed__7_MoveNext_m2F362F6C1566FAF86BBB004FA8C55B06DFF6F8E8 ();
// 0x0000002D System.Object ShipController_<TransitionToWinScene>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransitionToWinSceneU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67E73737FB02BBC65FAF9EE079B7B1E54BC75987 ();
// 0x0000002E System.Void ShipController_<TransitionToWinScene>d__7::System.Collections.IEnumerator.Reset()
extern void U3CTransitionToWinSceneU3Ed__7_System_Collections_IEnumerator_Reset_mA192A9A04F0B6418259CB20098DC46CECF99F313 ();
// 0x0000002F System.Object ShipController_<TransitionToWinScene>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CTransitionToWinSceneU3Ed__7_System_Collections_IEnumerator_get_Current_mEE36E3D6DFF5F530A463DFC90BEE38383E826E68 ();
static Il2CppMethodPointer s_methodPointers[47] = 
{
	HoldButton_Start_mBF83F0E934C0BC78B4A675D31C328B818B8BA8AA,
	HoldButton_OnPointerDown_m1B7CCEE563D8DE2E9966D7B765DBF44EB173B844,
	HoldButton_OnPointerUp_m4A244144ABF324D95F2AFB5A4D0E9A4C49B42BF7,
	HoldButton_isPressed_m837C8FEE84F921E92091CD5628907676C2155F09,
	HoldButton__ctor_m7AD08A6AAABDA0A3E8B09A6F08C5AF8E4448CC69,
	ImageChangingButton_Start_m88DD1213E90EB79DA3AD9D6588ACAFDD60FCD744,
	ImageChangingButton_OnPointerDown_mBA84458244A8291BCDF29EE96918FACDC876A745,
	ImageChangingButton_OnPointerUp_m224607CA1FBC7BA237D8119AE636D135670C530C,
	ImageChangingButton__ctor_mA7DEAE869B41C2CE0937A4FD8B21B00E24BCFF67,
	MusicPlayer_get_Instance_mBF96BD3397BC310FA231B83F358086DCA224E855,
	MusicPlayer_Awake_mB58A606103D324532830131CA4A73C00B99E4C7A,
	MusicPlayer_PlayMusic_mE3F3ECCD025172827E90D20919C0699D86EE90CD,
	MusicPlayer_StopMusic_mF6DD0067F9A3730D289A83E0B4F2519DFFE676F0,
	MusicPlayer__ctor_mE89B163EA72BF12E0B7D28E30407AF24E94741BF,
	MusicPlayer__cctor_m01C6940307C44F6B6CC29C16CED4CBF995DE8734,
	PlayerController_Start_mC0C9B9461D0BDAC48EC43715818A4BA63C4F45EF,
	PlayerController_Update_m38903EF1C8F12B9388303741F8040EE26C33DC33,
	PlayerController_accelerate_m13F44234BA301D6859140FE35B3886BE4DDCEC0D,
	PlayerController_rotate_m9B5E181B0F5BD743896C5C07D55F7E6FCEFC97F7,
	PlayerController_handleHolding_mA03FA819CA48D04CDA3E2FE87820D3C6D13598EF,
	PlayerController__ctor_m648E40092E37395F4307411E855445886113CD60,
	SceneController_Start_m1A1FAC171D097231B7D7A3BEE3BC93FF49F759FF,
	SceneController_Update_m0C714789D1D87A7B86C9684D036F9A2E7F9B338E,
	SceneController_ResetScene_mD33EFE9411F4D88B36520386561D7EC914F6D39A,
	SceneController__ctor_mDE5EC139B719C6585BDFA1AE15234C5848FE8D01,
	ShipConnectionPoint_Start_mA21AADE789A0EEE98518AC663FE0E919A2F44BA3,
	ShipConnectionPoint_Update_mF125726E3569EF0C185AAAA46EB24C9E658A2A14,
	ShipConnectionPoint_isConnectedToCorrespondingPoint_m84D1AA9321DF5179185D1CA91D451E928228CB93,
	ShipConnectionPoint__ctor_mFA1903D40E9AE3A80E0D335B4B61F42ABF18BB80,
	ShipController_Start_m6AEAA5CD9EA713B204194B3A8FFA90E7142A9B8A,
	ShipController_Update_m9413942EDB9440735D42B115F999B0924ED8D086,
	ShipController_TransitionToWinScene_m11B19D0C80DA56BF6557BD0AA8CE23F1423CCD4A,
	ShipController__ctor_m8090CC282CD23B0D2C8A090E263A00EF44070366,
	SlideBarControl_OnPointerUp_m54FD6F19FE26D4A76BCF477ED6F5CBF48AD58691,
	SlideBarControl_GetValue_m05069915FECAC42AE48F3472C5B133A1CB4A83B9,
	SlideBarControl_Start_mC9193599DE8A112647B4550FFE2149D2B5E25F00,
	SlideBarControl__ctor_mF47A8579854AFF144622CC98576B9F3D56E16CB3,
	TitleScreenController_PlayGame_m6540FD2BF81AF7ED8E3C7982DCE2EE64C98ECA45,
	TitleScreenController_GoToCredits_m7E10B65732483BDF2AB8F505D501827CAD04C791,
	TitleScreenController_GoToTitleScreen_mE6201EC69F7C96933820712FE977EDF7C48BF19E,
	TitleScreenController__ctor_m725EE47F44118059E3FF7FC794CE549B0DC0DED1,
	U3CTransitionToWinSceneU3Ed__7__ctor_m0037C51497BD3C27A8E1C69519026A6144286819,
	U3CTransitionToWinSceneU3Ed__7_System_IDisposable_Dispose_mF76A0F8AD4AA67C2A0D93801A152CA50C3CBA7E9,
	U3CTransitionToWinSceneU3Ed__7_MoveNext_m2F362F6C1566FAF86BBB004FA8C55B06DFF6F8E8,
	U3CTransitionToWinSceneU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67E73737FB02BBC65FAF9EE079B7B1E54BC75987,
	U3CTransitionToWinSceneU3Ed__7_System_Collections_IEnumerator_Reset_mA192A9A04F0B6418259CB20098DC46CECF99F313,
	U3CTransitionToWinSceneU3Ed__7_System_Collections_IEnumerator_get_Current_mEE36E3D6DFF5F530A463DFC90BEE38383E826E68,
};
static const int32_t s_InvokerIndices[47] = 
{
	23,
	26,
	26,
	114,
	23,
	23,
	26,
	26,
	23,
	4,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	289,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	23,
	23,
	23,
	14,
	23,
	26,
	677,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	47,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
