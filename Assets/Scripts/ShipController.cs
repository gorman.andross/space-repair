﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShipController : MonoBehaviour
{

    private ShipConnectionPoint[] _connectionPoints;

    public Rigidbody2D leftHalf;
    public Rigidbody2D rightHalf;

    private AudioSource _winSound;
    private bool _isSolved;
    // Start is called before the first frame update
    void Start()
    {
        _connectionPoints = gameObject.GetComponentsInChildren<ShipConnectionPoint>();
        _isSolved = false;
        _winSound = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isSolved)
        {
            foreach (ShipConnectionPoint point in _connectionPoints) 
            {
                if (!point.isConnectedToCorrespondingPoint()) {
                    return;
                }
            }
            _isSolved = true;
            Joint2D joint = leftHalf.gameObject.AddComponent<FixedJoint2D>();
            joint.connectedBody = rightHalf;
            _winSound.Play();
            StartCoroutine(TransitionToWinScene());
        }
        
    }

    private IEnumerator TransitionToWinScene()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("WinScene");
    }
}
