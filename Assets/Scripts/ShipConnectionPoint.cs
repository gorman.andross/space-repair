﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipConnectionPoint : MonoBehaviour
{
    public Collider2D correspondingPoint;

    private Collider2D _collider;

    // Start is called before the first frame update
    void Start()
    {
        _collider = gameObject.GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool isConnectedToCorrespondingPoint() {
        float distance = _collider.Distance(correspondingPoint).distance;
        return distance < 0.25f;
    }
}
