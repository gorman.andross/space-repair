﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ImageChangingButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Sprite pressedButtonSprite;

    private Button _button;
    private Sprite _unpressedButtonSprite;

    void Start()
    {
        _button = GetComponent<Button>();
        _unpressedButtonSprite = _button.image.sprite;
    }

    public void OnPointerDown(PointerEventData data)
    {
        _button.image.sprite = pressedButtonSprite;

    }

    public void OnPointerUp(PointerEventData data)
    {
        _button.image.sprite = _unpressedButtonSprite;
    }
}
