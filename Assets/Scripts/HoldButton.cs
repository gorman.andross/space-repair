using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class HoldButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private bool _isPressed;

    void Start()
    {
        _isPressed = false;
    }
    
    public void OnPointerDown(PointerEventData data) 
    {
        _isPressed = true;
    }

    public virtual void OnPointerUp(PointerEventData data)
    {
        _isPressed = false;
    }

    public bool isPressed() {
        return _isPressed;
    }
}