﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public HoldButton thrustButton;
    public HoldButton grabButton;
    public SlideBarControl slideBarControl;


    private Joint2D _joint;
    private Rigidbody2D rb2D;

    private Collider2D physicsCollider;
    private readonly float thrust = 10.0f;
    private readonly float torque = 0.1f;

    private GameObject[] shipPieces;

    // Start is called before the first frame update
    void Start()
    {
        rb2D = gameObject.GetComponent<Rigidbody2D>();
        physicsCollider = gameObject.GetComponent<Collider2D>();
        shipPieces = GameObject.FindGameObjectsWithTag("Ship");
    }

    // Update is called once per frame
    void Update()
    {
        if (thrustButton.isPressed()) 
        {
            accelerate();
        }

        if (slideBarControl.isPressed()) {
            rotate(slideBarControl.GetValue());
        }

        handleHolding();
    }

    private void accelerate() 
    {
        rb2D.AddForce(transform.right * thrust);
    }

    private void rotate(float power)
    {
        rb2D.AddTorque(power * torque);
    }

    private void handleHolding() 
    {
        if (_joint == null && grabButton.isPressed()) {
            foreach (GameObject shipPiece in shipPieces) {
                Collider2D shipCollider =  shipPiece.GetComponent<Collider2D>();
                ColliderDistance2D distance = physicsCollider.Distance(shipCollider);
                if (distance.distance <= 0.5f) {
                    _joint = gameObject.AddComponent<FixedJoint2D>();
                    _joint.connectedBody = shipPiece.GetComponent<Rigidbody2D>();
                    return;
                }
            }
        } else if (_joint != null && !grabButton.isPressed()) {
            FixedJoint2D joint = gameObject.GetComponent<FixedJoint2D>();
            joint.connectedBody = null;
            Destroy(joint);
        }
        
    }
}
