﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SlideBarControl : HoldButton
{
    private Slider slider;

    public override void OnPointerUp(PointerEventData data) 
    {
        base.OnPointerUp(data);
        slider.value = 0;
    }

    public float GetValue()
    {
        return slider.value;
    }

    void Start()
    {
        slider = gameObject.GetComponent<Slider>();
    }
}
