﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreenController : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("GameplayScene");
    }

    public void GoToCredits()
    {
        SceneManager.LoadScene("CreditScene");
    }

    public void GoToTitleScreen()
    {
        SceneManager.LoadScene("TitleScreenScene");
    }

}
